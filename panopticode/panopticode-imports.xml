<!--
 Copyright (c) 2006-2007 Julias R. Shaw

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.
-->
<project name="Panopticode Imports">
    <taskdef resource="net/sf/antcontrib/antcontrib.properties">
      <classpath>
        <pathelement location="${panopticodeDir}/lib/ant-contrib.jar"/>
      </classpath>
    </taskdef>
    
	<import file="${panopticodeDir}/supplements/checkstyle/checkstyle-imports.xml" />
	<import file="${panopticodeDir}/supplements/complexian/complexian-imports.xml" />
	<import file="${panopticodeDir}/supplements/javancss/javancss-imports.xml" />
	<import file="${panopticodeDir}/supplements/jdepend/jdepend-imports.xml" />
	<import file="${panopticodeDir}/supplements/deadcode/deadcode-imports.xml" />
	<!--
        Panopticode supports multiple competing code coverage tools.  You can switch which tool you use at will and your
        build files will not change.  You must choose exactly one though.  If you do not want to collect coverage data
        then choose the 'nocoverage' import.  You choose your coverage tool by using exactly one of the following import
        statements:

            <import file="plugins/emma/emma-imports.xml" />
            <import file="plugins/cobertura/cobertura-imports.xml" />
            <import file="plugins/nocoverage-imports.xml" />
        The default is Emma.  Try them all to see which one you prefer.
    -->
	<import file="${panopticodeDir}/supplements/jacoco/jacoco-imports.xml" />

	<!--
        Panopticode plans to support multiple source code management tools.  If your tool is listed below then select
        its import.  If you are not using one of the SCM tools listed below choose the 'noscm' import.  You choose your
        SCM tool by using exactly one of the following import statements:

            <import file="${panopticodeDir}/supplements/subversion/subversion-imports.xml" />
            <import file="${panopticodeDir}/supplements/noscm-imports.xml" />

        The default is Subversion.
    -->
    <import file="${panopticodeDir}/supplements/git/git-imports.xml" />

	<macrodef name="panopticode">
		<attribute name="panopticodeDir" />
		<attribute name="projectDir" />
		<attribute name="projectName" />
		<attribute name="projectVersion" />
		<attribute name="outputDir" />
		<attribute name="classDir" default="bin" />
		<attribute name="srcDir" default="src" />
		<attribute name="testDir" default="tests" />
        <attribute name="churnDuration" />

		<sequential>
			<javadoc classpath="@{panopticodeDir}/panopticode.jar" sourcepath="@{srcDir}" access="private">
				<fileset dir="@{srcDir}" />
				<doclet name="org.panopticode.doclet.PanopticodeDoclet" path="@{panopticodeDir}/panopticode.jar">
					<param name="-debug" value="true" />
					<param name="-projectName" value="@{projectName}" />
					<param name="-projectVersion" value="@{projectVersion}" />
					<param name="-outputFile" value="@{outputDir}/panopticode.xml" />
				</doclet>
			</javadoc>

			<!-- Run any metrics that work directly on source code (i.e. They don't need compiled code) here -->
			<panopticode-javancss panopticodeDir="@{panopticodeDir}" outputDir="@{outputDir}" srcDir="@{srcDir}" testDir="@{testDir}" />

			<taskdef name="javancssPanopticode" classpath="@{panopticodeDir}/panopticode.jar" classname="org.panopticode.supplement.ant.JavaNCSSPanopticode" />
			<javancssPanopticode panopticodeFile="@{outputDir}/panopticode.xml" javancssFile="@{outputDir}/rawmetrics/xml/javancss.xml" />

			<panopticode-checkstyle panopticodeDir="@{panopticodeDir}" configFile="@{panopticodeDir}/supplements/checkstyle/etc/panopticode_checks.xml" outputDir="@{outputDir}" srcDir="@{srcDir}" />

			<!-- Time to compile -->
			<antcall target="compile" />

			<panopticode-complexian panopticodeDir="@{panopticodeDir}" outputDir="@{outputDir}" srcDir="@{srcDir}" />

			<!-- Run any metrics that require compiled code here -->
			<!-- JDepend (We want to run this just after compile so we don't gather metrics on extra code -->
			<panopticode-jdepend panopticodeDir="@{panopticodeDir}" outputDir="@{outputDir}" classDir="@{classDir}" />
			<taskdef name="jdependPanopticode" classpath="@{panopticodeDir}/panopticode.jar" classname="org.panopticode.supplement.ant.JDependPanopticode" />

			<jdependPanopticode panopticodeFile="@{outputDir}/panopticode.xml" jdependFile="@{outputDir}/rawmetrics/xml/jdepend.xml" />

			<!-- Coverage -->
			<!-- <panopticode-setup-coverage outputDir="@{outputDir}" classDir="@{classDir}" /> -->
			<antcall target="unit-test" />
		    <echo message="Finished running Unit Tets" />
			<panopticode-report-coverage srcDir="@{srcDir}" classDir="@{classDir}" outputDir="@{outputDir}" panopticodeDir="@{panopticodeDir}" />

			<taskdef name="panopticodeJacoco" classpath="@{panopticodeDir}/panopticode.jar" classname="org.panopticode.supplement.ant.JacocoPanopticode" />
			<panopticodeJacoco panopticodeFile="@{outputDir}/panopticode.xml" jacocoFile="@{outputDir}/rawmetrics/xml/jacoco.xml" />
			
			<deadcode panopticodeDir="@{panopticodeDir}" outputDir="@{outputDir}" classDir="@{classDir}" />
		    
		    <taskdef name="gitChurnPanopticode" classpath="@{panopticodeDir}/panopticode.jar" classname="org.panopticode.supplement.ant.GitChurnPanopticode" />

            <git-log duration="@{churnDuration}" dir="@{srcdir}" gitLog="@{outputDir}/git.log" />
            <gitChurnPanopticode panopticodeFile="@{outputDir}/panopticode.xml" duration="@{churnDuration}" gitLog="@{outputDir}/git.log" />

            <taskdef name="c3Panopticode" classpath="@{panopticodeDir}/panopticode.jar" classname="org.panopticode.supplement.ant.C3Panopticode" />
            <!-- This should be the last task run as it dependes on execution of other supplement tasks -->
            <c3Panopticode panopticodeFile="@{outputDir}/panopticode.xml" />
		    
			<!-- Run the reports -->
			<mkdir dir="@{outputDir}/reports/svg" />

			<echo message="Building Static Reports" />

			<report pluginClass="org.panopticode.report.treemap.ComplexityTreemap" panopticodeDir="@{panopticodeDir}" panopticodeXmlFile="@{outputDir}/panopticode.xml" file="@{outputDir}/reports/svg/complexity-treemap.svg" />

			<report pluginClass="org.panopticode.report.treemap.CoverageTreemap" panopticodeDir="@{panopticodeDir}" panopticodeXmlFile="@{outputDir}/panopticode.xml" file="@{outputDir}/reports/svg/coverage-treemap.svg" />

            <report pluginClass="org.panopticode.report.treemap.C3Treemap" panopticodeDir="@{panopticodeDir}" panopticodeXmlFile="@{outputDir}/panopticode.xml" file="@{outputDir}/reports/svg/c3-treemap.svg" />

            <report pluginClass="org.panopticode.report.treemap.ChurnTreemap" panopticodeDir="@{panopticodeDir}" panopticodeXmlFile="@{outputDir}/panopticode.xml" file="@{outputDir}/reports/svg/churn-treemap.svg" />

			<echo message="Building Interactive Reports" />

			<report pluginClass="org.panopticode.report.treemap.ComplexityTreemap" panopticodeDir="@{panopticodeDir}" panopticodeXmlFile="@{outputDir}/panopticode.xml" file="@{outputDir}/reports/svg/interactive-complexity-treemap.svg" interactive="-interactive" />

			<report pluginClass="org.panopticode.report.treemap.CoverageTreemap" panopticodeDir="@{panopticodeDir}" panopticodeXmlFile="@{outputDir}/panopticode.xml" file="@{outputDir}/reports/svg/interactive-coverage-treemap.svg" interactive="-interactive" />
		    
		    <report pluginClass="org.panopticode.report.treemap.C3Treemap" panopticodeDir="@{panopticodeDir}" panopticodeXmlFile="@{outputDir}/panopticode.xml" file="@{outputDir}/reports/svg/interactive-c3-treemap.svg" interactive="-interactive" />

            <report pluginClass="org.panopticode.report.treemap.ChurnTreemap" panopticodeDir="@{panopticodeDir}" panopticodeXmlFile="@{outputDir}/panopticode.xml" file="@{outputDir}/reports/svg/interactive-churn-treemap.svg" interactive="-interactive" />
		</sequential>
	</macrodef>

	<macrodef name="report">
		<attribute name="panopticodeDir" />
		<attribute name="pluginClass" />
		<attribute name="panopticodeXmlFile" />
		<attribute name="file" />
		<attribute name="interactive" default="" />

		<sequential>
			<java classpath="@{panopticodeDir}/panopticode.jar" classname="org.panopticode.ReportRunner" fork="true">
				<arg value="@{pluginClass}" />
				<arg value="@{panopticodeXmlFile}" />
				<arg value="@{file}" />
				<arg value="@{interactive}" />
			</java>
		</sequential>
	</macrodef>

	<target name="rasterize-all-reports" depends="rasterize-png-reports,rasterize-jpeg-reports,rasterize-tiff-reports" />

	<target name="rasterize-png-reports">
		<delete dir="@{outputDir}/reports/png" />
		<mkdir dir="@{outputDir}/reports/png" />

		<svg-to-png srcFile="@{outputDir}/reports/svg/complexity-treemap.svg" destFile="@{outputDir}/reports/png/complexity-treemap.png" />
		<svg-to-png srcFile="@{outputDir}/reports/svg/coverage-treemap.svg" destFile="@{outputDir}/reports/png/coverage-treemap.png" />

		<svg-to-png srcFile="@{outputDir}/reports/svg/complexity-treemap.svg" destFile="@{outputDir}/reports/png/complexity-treemap-thumb.png" width="128" height="96" />
		<svg-to-png srcFile="@{outputDir}/reports/svg/coverage-treemap.svg" destFile="@{outputDir}/reports/png/coverage-treemap-thumb.png" width="128" height="96" />
	</target>

	<target name="rasterize-jpeg-reports">
		<delete dir="@{outputDir}/reports/jpeg" />
		<mkdir dir="@{outputDir}/reports/jpeg" />

		<svg-to-jpeg srcFile="@{outputDir}/reports/svg/complexity-treemap.svg" destFile="@{outputDir}/reports/jpeg/complexity-treemap.jpg" />
		<svg-to-jpeg srcFile="@{outputDir}/reports/svg/coverage-treemap.svg" destFile="@{outputDir}/reports/jpeg/coverage-treemap.jpg" />

		<svg-to-jpeg srcFile="@{outputDir}/reports/svg/complexity-treemap.svg" destFile="@{outputDir}/reports/jpeg/complexity-treemap-thumb.jpg" width="128" height="96" />
		<svg-to-jpeg srcFile="@{outputDir}/reports/svg/coverage-treemap.svg" destFile="@{outputDir}/reports/jpeg/coverage-treemap-thumb.jpg" width="128" height="96" />
	</target>

	<target name="rasterize-tiff-reports">
		<delete dir="@{outputDir}/reports/tiff" />
		<mkdir dir="@{outputDir}/reports/tiff" />

		<svg-to-tiff srcFile="@{outputDir}/reports/svg/complexity-treemap.svg" destFile="@{outputDir}/reports/tiff/complexity-treemap.tif" />
		<svg-to-tiff srcFile="@{outputDir}/reports/svg/coverage-treemap.svg" destFile="@{outputDir}/reports/tiff/coverage-treemap.tif" />

		<svg-to-tiff srcFile="@{outputDir}/reports/svg/complexity-treemap.svg" destFile="@{outputDir}/reports/tiff/complexity-treemap-thumb.tif" width="128" height="96" />
		<svg-to-tiff srcFile="@{outputDir}/reports/svg/coverage-treemap.svg" destFile="@{outputDir}/reports/tiff/coverage-treemap-thumb.tif" width="128" height="96" />
	</target>
</project>
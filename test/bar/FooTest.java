package bar;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FooTest {

    @Test
    public void testHello() {
        assertEquals("hello", new Foo().hello(false));
    }

}
